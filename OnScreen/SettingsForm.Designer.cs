﻿namespace OnScreen
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_driver = new System.Windows.Forms.Label();
            this.listBox_driver = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label_driver
            // 
            this.label_driver.AutoSize = true;
            this.label_driver.Location = new System.Drawing.Point(41, 105);
            this.label_driver.Name = "label_driver";
            this.label_driver.Size = new System.Drawing.Size(35, 13);
            this.label_driver.TabIndex = 0;
            this.label_driver.Text = "Driver";
            this.label_driver.Click += new System.EventHandler(this.label_driver_Click);
            // 
            // listBox_driver
            // 
            this.listBox_driver.FormattingEnabled = true;
            this.listBox_driver.Location = new System.Drawing.Point(100, 105);
            this.listBox_driver.Name = "listBox_driver";
            this.listBox_driver.Size = new System.Drawing.Size(109, 17);
            this.listBox_driver.TabIndex = 1;
            this.listBox_driver.SelectedIndexChanged += new System.EventHandler(this.listBox_driver_SelectedIndexChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.listBox_driver);
            this.Controls.Add(this.label_driver);
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HideWindow);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_driver;
        private System.Windows.Forms.ListBox listBox_driver;
    }
}