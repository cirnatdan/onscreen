﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnScreen
{
    public partial class TopForm : Form
    {
        private const int WM_NCLBUTTONDOWN = 0xA1;

        private System.Threading.Timer timer;

        /// <summary>
        /// used for thread safe calling of getFocus()
        /// </summary>
        delegate void GetFocusCallback();

        public TopForm()
        {
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.FormBorderStyle = FormBorderStyle.None;
        }

        public void Display()
        {
            this.WindowState = FormWindowState.Normal;
            int y = Screen.PrimaryScreen.Bounds.Bottom - this.Height;
            this.Location = new Point(0, y);
            //make window on top
            this.TopMost = true;
            this.Activate();

            timer = new System.Threading.Timer(_ => getFocus(), null, 1000 * 10, Timeout.Infinite);
        }

        /// <summary>
        /// Thread safe method for acquiring focus
        /// </summary>
        public void getFocus()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new GetFocusCallback(getFocus));
            }
            else
            {
                this.TopMost = true;
                //this.Refresh();
                Thread.Yield();
                Thread.Sleep(1000);
                timer.Change(1000 * 10, Timeout.Infinite);
            }
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            this.TopMost = true;
            base.OnVisibleChanged(e);
        }
    
        //protected override void OnMouseDown(MouseEventArgs e)
        //{
        //    base.OnMouseDown(e);
        //    // allow window moving
        //    if (e.Button == MouseButtons.Left)
        //    {
        //        this.Capture = false;
        //        Message msg = Message.Create(this.Handle, WM_NCLBUTTONDOWN, new IntPtr(2), IntPtr.Zero);
        //        this.WndProc(ref msg);
        //    }
        //}

        public void OnExit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        protected void DisposeResources(bool isDisposing)
        {
            if (isDisposing)
            {
                // Release the icon resource.
                //trayIcon.Dispose();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
