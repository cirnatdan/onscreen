﻿using System.Windows.Forms;

namespace OnScreen
{
    interface IKeyboardListener
    {
        void onKey(Keys key);
    }
}
