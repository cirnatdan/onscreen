﻿using System;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnScreen
{
    static class Program
    {
        private static TopForm topForm;
        private static NotifyIcon trayIcon;
        private static ContextMenu trayMenu;
        private static SettingsForm settingsForm;

        private static KeyboardMonitor keyMonitor;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            topForm = new TopForm();
            settingsForm = new SettingsForm();

            keyMonitor = new KeyboardMonitor();
            DisplayWindowListener listener = new DisplayWindowListener(topForm);
            keyMonitor.AddListener(listener.onKey);
            InitTrayIcon();
            Application.Run(topForm);
        }

        public static void InitTrayIcon()
        {
            trayIcon = new NotifyIcon();
            trayIcon.Text = "OnScreen";
            trayIcon.Icon = new Icon(SystemIcons.Application, 40, 40);
            trayIcon.Visible = true;

            trayMenu = new ContextMenu();
            trayMenu.MenuItems.Add("Exit", topForm.OnExit);
            trayMenu.MenuItems.Add("Settings", settingsForm.OnMenuClick);
            trayIcon.ContextMenu = trayMenu;
        }

        internal class DisplayWindowListener : IKeyboardListener
        {
            private TopForm window;
            private const string combination = "1111111111111111";
            private string keys = "";
            private KeysConverter keyConverter = new KeysConverter();

            public DisplayWindowListener(TopForm window)
            {
                this.window = window;
            }

            public void onKey(Keys key)
            {
                keys += keyConverter.ConvertToString(key);
                if (keys.Length == 16 && keys == combination)
                {
                    window.Display();
                }
            }
        }
    }
}
